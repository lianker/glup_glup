def get_bottle_with_same_volume(volume, bottles):
    b = [bottle for bottle in bottles if bottle['volume'] == volume]
    if len(b) > 0:
        bottles.remove(b[0])
        return (b[0])

    return False


def get_highest_bottle(volume, bottles):
    bottle_list = [b for b in bottles if b['volume'] < volume]
    if len(bottle_list) > 0:
        bottle_list.sort(key=lambda b: b['volume'])
        highest_bottle = bottle_list[-1]
        bottles.remove(highest_bottle)
        return highest_bottle

    return False


def fills_the_volume(volume, bottles):
    bottle_list = [b for b in bottles if b['rest'] > 0]
    if len(bottle_list) > 0:
        bottle_list.sort(key=lambda b: b['rest'])
        chosen = bottle_list[0]
        bottles.remove(chosen)
        return chosen

    return False


def define_rests(volume, bottles):
    return list(
        map(lambda b: {"volume": b, "rest": round(b - volume, 1)}, bottles))


def pick_bottle(volume, bottles_volume_list):
    bottles = define_rests(volume, bottles_volume_list)

    chosen_bottle = (get_bottle_with_same_volume(volume, bottles)
                     or get_highest_bottle(volume, bottles)
                     or fills_the_volume(volume, bottles))

    result_list = list(map(lambda b: b['volume'], bottles))
    return (chosen_bottle['volume'], result_list)


def fill_gallon(total_volume, bottles):
    used_bottles = []
    initial_volume = total_volume
    
    if total_volume - sum(bottles) > 0:
        return (bottles, round(sum(bottles) - total_volume, 1))
    
    while sum(used_bottles) < initial_volume:
        total_volume = total_volume - sum(used_bottles)
        b, new_list = pick_bottle(total_volume, bottles)

        used_bottles.append(b)
        bottles = new_list

    return (used_bottles, round(sum(used_bottles) - initial_volume, 1))
