from glup_glup import fill_gallon


def main():
    gallon_vol = float(input("Volume do galão\n"))
    number_of_bottles = input("Quantidade de garrafas\n")
    bottles = []

    for i in range(int(number_of_bottles)):
        bottle_vol = input('Digite o valor da garrafa {}\n'.format(i+1))
        bottles.append(float(bottle_vol))

    chosen_bottles, rest = fill_gallon(gallon_vol, bottles)
    chosen_bottles = list(
        map(lambda litros: '{}L'.format(litros), chosen_bottles))

    print('Resposta: {}, sobra {}'.format(chosen_bottles, rest))


if __name__ == "__main__":
    try:
        main()
    except:
        print("Entrada inválida. Use apenas números")
        main()
