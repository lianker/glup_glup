# Glup Glup

Este repositório se destina a criar um módulo que resolva o problema a seguir:

Dado um conjunto de garrafas d'água, com volumes de água diferentes entre si, e um galão de água. Crie um algoritmo, para escolher as garrafas a serem utilizadas para encher o galão, de acordo:

1. O galão deve ser completamente preenchido com o volume das garrafas;
2. Procure esvaziar totalmente as garrafas escolhidas;
3. Quando não for possível esvaziar todas garrafas escolhidas, deixe a menor sobra possível;
4. utilize o menor número de garrafas possível;

## Dependências

- Python 3.7

## Testes

```
python test_glup_glup.py
```

## Rodando o exemplo

```
python main.py
```

> Ao longo do desenvolvimento, algumas soluções de algoritmo tiveram início, mas depois sofreram grandes reformulações para que o resultado final resolvesse o problema em questão levando em conta todas os requisitos, na ordem especificada. As soluções anteriores foram mantidas em outras branchs, respectivamente `wip/solution_01` e `wip/solution_02`