import unittest

from glup_glup import *


class FillsTheVolumeTest(unittest.TestCase):
    def test_get_bottle_that_fills_gallon_with_minor_rest(self):
        vol = 3.2
        bottles = define_rests(vol, [3.4, 3.5])

        chosen_bottle = fills_the_volume(vol, bottles)
        self.assertEqual(3.4, chosen_bottle['volume'])

    def test_remove_item_from_list(self):
        vol = 3.2
        bottles = define_rests(vol, [3.4, 3.5, 2.0])

        chosen_bottle = fills_the_volume(vol, bottles)
        expected = define_rests(vol, [3.5, 2.0])
        self.assertEqual(3.4, chosen_bottle['volume'])
        self.assertCountEqual(expected, bottles)

    def test_return_false_if_no_items_found(self):
        vol = 3.2
        bottles = define_rests(vol, [3.1, 2.0])

        result = fills_the_volume(vol, bottles)
        self.assertFalse(result)


class HighestBottleTest(unittest.TestCase):
    def test_get_highest_bottle_with_volume_less_then_target_volume(self):
        target_volume = 7.0
        bottles = define_rests(target_volume, [7.0, 32.0, 5.0, 4.0])

        chosen_bottle = get_highest_bottle(target_volume, bottles)
        self.assertLess(chosen_bottle['volume'], target_volume)
        self.assertEqual(5.0, chosen_bottle['volume'])

    def test_remove_item_with_volume_5(self):
        target_volume = 7.0
        bottles = define_rests(target_volume, [7.0, 32.0, 5.0, 4.0])

        chosen_bottle = get_highest_bottle(target_volume, bottles)

        expected = define_rests(target_volume, [7.0, 32.0, 4.0])
        self.assertCountEqual(expected, bottles)

    def test_return_false_if_any_item_found(self):
        target_volume = 3.0
        bottles = define_rests(target_volume, [7.0, 32.0, 5.0, 4.0])

        result = get_highest_bottle(target_volume, bottles)

        self.assertFalse(result)


class SameVolumeTest(unittest.TestCase):
    def test_get_item_with_volume_10(self):
        v = 10.0
        bottles = define_rests(v, [10.0, 10.1, 2.0])

        chosen_bottle = get_bottle_with_same_volume(v, bottles)
        self.assertEqual(10.0, chosen_bottle['volume'])

    def test_remove_item_with_volume_10(self):
        v = 10.0
        bottles = define_rests(v, [10.0, 10.1, 2.0])

        get_bottle_with_same_volume(v, bottles)

        expected = define_rests(v, [10.1, 2.0])
        self.assertCountEqual(expected, bottles)

    def test_return_false_if_any_item_found(self):
        v = 10.0
        bottles = define_rests(v, [15.0, 10.1, 2.0])

        result = get_bottle_with_same_volume(v, bottles)

        self.assertFalse(result)


class FillGallonTest(unittest.TestCase):
    def test_sample_01(self):
        bottles, rest = fill_gallon(7.0, [1.0, 3.0, 4.5, 1.5, 3.5])
        self.assertEqual(0.0, rest)
        self.assertListEqual(sorted([4.5, 1.0, 1.5]), sorted(bottles))

    def test_sample_02(self):
        bottles, rest = fill_gallon(5.0, [1.0, 3.0, 4.5, 1.5])
        self.assertEqual(0.5, rest)
        self.assertListEqual(sorted([4.5, 1.0]), sorted(bottles))

    def test_sample_03(self):
        bottles, rest = fill_gallon(4.9, [4.5, 0.4])
        self.assertEqual(0.0, rest)
        self.assertListEqual(sorted([4.5, 0.4]), sorted(bottles))

    def test_sample_04(self):
        bottles, rest = fill_gallon(13.9, [14.0, 12.5, 13.8, 14.1])
        self.assertEqual(12.4, rest)
        self.assertListEqual(sorted([12.5, 13.8]), sorted(bottles))


if __name__ == "__main__":
    unittest.main()
